# frozen_string_literal: true

# Placeholder for https://gitlab.com/gitlab-org/gitlab-ee/merge_requests/7341
# Added to resolve https://gitlab.com/gitlab-org/gitlab-ee/merge_requests/8131
